extends StateMachine
class_name PlayerStateMachine

const RUN_THRESHOLD = 64
const WALL_SLIDE_DISMOUNT_DISTANCE = 200
const JUMP_HEIGHT_WALL_SLIDE_OFFSET = 48

onready var landing_particles = preload('res://scenes/LandingParticles.tscn')

func _ready():
	add_state('idle')
	add_state('run')
	add_state('jump')
	add_state('fall')
	add_state('wall_slide')
	call_deferred('set_state', states.idle)

func _input(event):
	if state == states.wall_slide:
		if event.is_action_pressed('jump'):
			parent.wall_jump()
			set_state(states.jump)
	elif event.is_action_pressed('jump'):
		if [states.idle, states.run].has(state) or (state == states.fall and !parent.coyote_timer.is_stopped()):
			parent.coyote_timer.stop()
			parent.jump_initial_y = parent.get_position().y
			parent.velocity.y = -parent.jump_height

func _state_logic(delta):
	parent._update_move_direction()
	parent._update_wall_direction()
	
	if state == states.jump and parent.get_position().y <= parent.jump_initial_y - JUMP_HEIGHT_WALL_SLIDE_OFFSET:
		parent._handle_wall_slide_commitment()
	
	if state == states.fall:
		parent._handle_wall_slide_commitment()
	
	if state != states.wall_slide:
		parent._apply_movement()
		
	parent._apply_gravity(delta)
	
	if !parent.wall_slide_commitment_timer.is_stopped():
		parent._cap_gravity_wall_slide()
	
	if state == states.wall_slide:
		parent._cap_gravity_wall_slide()
		parent._handle_wall_jump()
		#parent._handle_wall_slide_sticky_timer()
	
	parent._apply_movement_vector(delta)
	if parent.GUN_ENABLED:
		parent._handle_shooting()

func _get_transition(delta):
	match state:
		states.idle:
			if !parent.is_on_floor():
				if parent.velocity.y < 0:
					return states.jump
				elif parent.velocity.y > 0:
					return states.fall
			elif parent.velocity.x != 0:
				if abs(parent.velocity.x) >= RUN_THRESHOLD:
					return states.run
		states.run:
			if !parent.is_on_floor():
				if parent.velocity.y < 0:
					return states.jump
				elif parent.velocity.y > 0:
					return states.fall
			elif abs(parent.velocity.x) < RUN_THRESHOLD:
				return states.idle
		states.jump:
			if parent.is_on_floor():
				return states.idle
			elif parent.velocity.y >= 0:
				return states.fall
		states.fall:
			if parent.is_on_floor():
				return states.idle
			elif parent.velocity.y < 0:
				return states.jump
		states.wall_slide:
			if parent.is_on_floor():
				return states.idle
			elif parent.wall_direction == 0:
				return states.fall
	return null
	
func _wall_slide_committed():
	# Wall slide commitment timer is finished, let's check if the player still wants to ride the wall
	if [states.jump, states.fall].has(state):
		set_state(states.wall_slide)

func _enter_state(new_state, old_state):
	match new_state:
		states.idle:
			parent.velocity.x = 0
			parent.player_sprite.play('default')
		states.run:
			parent.player_sprite.play('walking')
		states.jump:
			parent.player_sprite.play('jumping')
		states.fall:
			parent.player_sprite.play('falling')
			parent._handle_coyote_timer()
		states.wall_slide:
			parent.player_sprite.play('grabbing')
			parent.body.scale.x = parent.wall_direction

func _exit_state(old_state, new_state):
	match old_state:
		states.fall:
			if parent.is_on_floor():
				var lp = landing_particles.instance()
				get_node('/root/World').add_child(lp)
				var lp_pos = parent.get_position()
				lp_pos.y += 10 + parent.player_collider.shape.height / 2
				lp.set_position(lp_pos)
				lp.restart()
		states.wall_slide:
			parent.wall_slide_cooldown.start()
			parent.wall_jump_reticle.visible = false


func _on_WallSlideStickyTimer_timeout():
	if state == states.wall_slide:
		parent.velocity.x = -parent.wall_direction * WALL_SLIDE_DISMOUNT_DISTANCE
		set_state(states.fall)
