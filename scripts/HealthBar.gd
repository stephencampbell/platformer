extends Control

onready var health_bar = $HealthProgressBar

func _on_health_updated(health):
	health_bar.value = health
