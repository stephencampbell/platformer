extends KinematicBody2D

var velocity = Vector2()
onready var collider = $Collider
onready var damage_particles = preload('res://scenes/BulletContactParticles.tscn')

func _physics_process(delta):
	move_and_slide(velocity)
	for i in get_slide_count():
		var collision = get_slide_collision(i)
		if collision:
			var collider = collision.collider
			if collider.has_method('be_hit_by_bullet'):
				collider.be_hit_by_bullet(self)
			var collision_position = collision.get_position()
			var dp = damage_particles.instance()
			get_node('/root/World').add_child(dp)
			var dp_pos = get_position()
			dp.set_position(dp_pos)
			dp.restart()
			queue_free()

func set_velocity(vector):
	velocity = vector
