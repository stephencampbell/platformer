extends KinematicBody2D

signal health_updated(health)
signal killed()
signal grounded_updated(is_grounded)

const GUN_ENABLED = false

const SLOPE_STOP = 64
const MOVEMENT_SPRITE_ANGLE = 8

# Gravity
export (int) var gravity = 800
export (int) var terminal_velocity = 800
const DAMAGE_KNOCK = 400

# Movement
export (float) var acceleration = 0.08
export (int) var max_speed = 600
var movement_direction = 0

# Jumping
export (int) var jump_height = 400
export (int) var bounce_height = 400
var jump_initial_y = 0

# Health
export (float) var max_health = 100
onready var health = max_health setget _set_health

var velocity = Vector2()
var was_on_floor = false

onready var player_sprite = $Body/PlayerSprite
onready var player_collider = $PlayerCollider
onready var coyote_timer = $CoyoteTimer
onready var invulerability_timer = $InvulnerabilityTimer
onready var damage_animator = $DamageAnimator
onready var damage_particles = preload('res://scenes/DamageParticles.tscn')

onready var bounce_raycasts = $BounceRaycasts
const BOUNCE_MARGIN = 32

# Wall jumping
onready var left_wall_raycasts = $WallRaycasts/LeftWallRaycasts
onready var right_wall_raycasts = $WallRaycasts/RightWallRaycasts
onready var wall_slide_cooldown = $WallSlideCooldown
onready var wall_slide_sticky_timer = $WallSlideStickyTimer
onready var wall_slide_commitment_timer = $WallSlideCommitmentTimer
var wall_direction = 0
const WALL_JUMP_VELOCITY = Vector2(800, -500)
const WALL_JUMP_MAGNITUDE = 700
onready var wall_jump_reticle = $WallJumpReticle
var wall_jump_angle = null

onready var gun = $Body/Gun
onready var body = $Body
onready var camera = $PlayerCamera
onready var bullet = preload("res://scenes/Bullet.tscn")
onready var gun_cooldown = $GunCooldown

func _ready():
	for raycast in left_wall_raycasts.get_children():
		raycast.add_exception(self)
	for raycast in right_wall_raycasts.get_children():
		raycast.add_exception(self)
	for raycast in bounce_raycasts.get_children():
		raycast.add_exception(self)
	if GUN_ENABLED:
		gun.visible = true
		
func _process(delta):
	var mouse_pos = camera.get_global_mouse_position()
	gun.look_at(mouse_pos)

func _apply_gravity(delta):
	velocity.y += gravity * delta
	
	# Prevent exceeding terminal velocity
	velocity.y = clamp(velocity.y, -INF, terminal_velocity)

func _apply_movement():
	# Handle animation speed and direction
	player_sprite.speed_scale = abs(velocity.x) / max_speed
	#player_sprite.rotation_degrees = MOVEMENT_SPRITE_ANGLE * movement_direction
	if(movement_direction != 0):
		body.scale.x = movement_direction

	# Handle acceleration and max speed
	if wall_slide_cooldown.is_stopped():
		velocity.x = lerp(velocity.x, max_speed * movement_direction, acceleration)

func _update_move_direction():
	movement_direction = -int(Input.is_action_pressed('left')) + int(Input.is_action_pressed('right'))

func _handle_coyote_timer():
	if !is_on_floor() and was_on_floor:
		coyote_timer.start()
		
func _handle_wall_slide_sticky_timer():
	if movement_direction != 0 and movement_direction != wall_direction:
		if wall_slide_sticky_timer.is_stopped():
			wall_slide_sticky_timer.start()
	else:
		wall_slide_sticky_timer.stop()

func _apply_movement_vector(delta):
	if was_on_floor != is_on_floor():
		emit_signal('grounded_updated', is_on_floor())
	
	was_on_floor = is_on_floor()
	
	_check_bounce(delta)
	velocity = move_and_slide(velocity, Vector2(0, -1), SLOPE_STOP)

func damage(amount):
	if(invulerability_timer.is_stopped()):
		_set_health(health - amount)
		damage_animator.play("DamageAnimation")
		
		var dp = damage_particles.instance()
		get_node('/root/World').add_child(dp)
		var dp_pos = get_position()
		dp_pos.y -= player_collider.shape.height / 2
		dp.set_position(dp_pos)
		dp.restart()
		
		invulerability_timer.start()
	velocity.y = -DAMAGE_KNOCK

func kill():
	emit_signal('killed')

func _set_health(value):
	var prev_health = health
	health = clamp(value, 0, max_health)
	if health != prev_health:
		emit_signal('health_updated', health)
		if health == 0:
			kill()

func wall_jump():
	var wall_jump_velocity
	if(wall_jump_angle != null):
		var wall_jump_angle_radians = wall_jump_angle
		wall_jump_velocity = Vector2(cos(wall_jump_angle_radians), sin(wall_jump_angle_radians)) * WALL_JUMP_MAGNITUDE
	else:
		wall_jump_velocity = WALL_JUMP_VELOCITY
		wall_jump_velocity.x *= -wall_direction
	velocity = wall_jump_velocity

	
func _get_left_analogue_stick_angle():
	var deadzone = 0.5
	var x_axis = Input.get_joy_axis(0, JOY_AXIS_0)
	var y_axis = Input.get_joy_axis(0, JOY_AXIS_1)

	if abs(x_axis) > deadzone || abs(y_axis) > deadzone:
		return Vector2(x_axis, y_axis).angle()
	
	return null
	
func _handle_wall_jump():
	wall_jump_angle = _get_left_analogue_stick_angle()
	if(wall_jump_angle != null):
		wall_jump_reticle.visible = true
		wall_jump_reticle.rotation = wall_jump_angle
	else:
		wall_jump_reticle.visible = false

func _cap_gravity_wall_slide():
	var wall_slide_speed
	if wall_slide_commitment_timer.is_stopped():
		wall_slide_speed = 0
	else:
		wall_slide_speed = gravity / 20
	
	if velocity.y > 0:
		var max_velocity = wall_slide_speed # if !Input.is_action_pressed('down') else gravity
		velocity.y = min(velocity.y, max_velocity)
	else:
		velocity.y = max(velocity.y, -wall_slide_speed)
	

func _update_wall_direction():
	var is_near_wall_left = _check_is_valid_wall(left_wall_raycasts)
	var is_near_wall_right = _check_is_valid_wall(right_wall_raycasts)
	
	if is_near_wall_left and is_near_wall_right:
		wall_direction = movement_direction
	else:
		wall_direction = -int(is_near_wall_left) + int(is_near_wall_right)

func _check_is_valid_wall(wall_raycasts):
	for raycast in wall_raycasts.get_children():
		if raycast.is_colliding():
			# Get angle of colliding surface
			var dot = acos(Vector2.UP.dot(raycast.get_collision_normal()))
			if dot > PI * 0.35 and dot < PI * 0.55:
				return true
	return false

func _check_bounce(delta):
	if velocity.y > 0:
		for raycast in bounce_raycasts.get_children():
			raycast.cast_to = Vector2.DOWN * velocity * delta + Vector2.DOWN + Vector2(0, BOUNCE_MARGIN)
			raycast.force_raycast_update()
			if raycast.is_colliding() and raycast.get_collision_normal() == Vector2.UP:
				# Ready to bounce
				velocity.y = (raycast.get_collision_point() - raycast.global_position - Vector2.DOWN).y / delta
				raycast.get_collider().entity.call_deferred('be_bounced_upon', self)
				break

func bounce(bounce_velocity = bounce_height):
	velocity.y = -bounce_velocity
	
func _handle_shooting():
	if Input.is_action_pressed("shoot") and gun_cooldown.is_stopped():
		gun_cooldown.start()
		var bullet_instance = bullet.instance()
		var spawn_direction = get_global_mouse_position() - get_global_position()
		var spawn_point = get_position() + Vector2(54, 0).rotated(spawn_direction.angle())
		bullet_instance.set_position(spawn_point)
		bullet_instance.set_rotation(spawn_direction.angle())
		get_tree().get_root().get_node("World").add_child(bullet_instance)
		var spawn_vector = spawn_direction.normalized() * 3000
		bullet_instance.set_velocity(spawn_vector)

func _handle_wall_slide_commitment():
	if wall_direction != 0 and wall_direction == movement_direction and wall_slide_cooldown.is_stopped():
		if wall_slide_commitment_timer.is_stopped():
			# Intent to move onto wall, start wall slide commitment timer to check they really mean it
			wall_slide_commitment_timer.start()
	else:
		wall_slide_commitment_timer.stop()

