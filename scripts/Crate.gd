extends StaticBody2D

onready var crate_smash_particles = preload('res://scenes/CrateSmashParticles.tscn')

func smash():
	var csp = crate_smash_particles.instance()
	get_node('/root/World').add_child(csp)
	var csp_pos = get_position()
	#csp_pos.y -= player_collider.shape.height / 2
	csp.set_position(csp_pos)
	csp.restart()
	queue_free()

func be_bounced_upon(bouncer):
	if bouncer.has_method('bounce'):
		bouncer.bounce()
	smash()

func be_hit_by_bullet(bullet):
	smash()
