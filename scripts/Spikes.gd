extends Area2D

export (int) var spike_damage = 10

func _on_Spikes_area_entered(area):
	if area.get_name() == "PlayerArea":
		var p = area.get_parent()
		p.damage(spike_damage)
